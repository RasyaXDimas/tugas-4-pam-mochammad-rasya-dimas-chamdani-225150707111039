package com.example.recyclerview.room

import android.content.Context
import androidx.room.Database
import androidx.room.*
import com.example.recyclerview.room.Menu

//import androidx.room.RoomDatabase

@Database(
    entities = [Menu::class],
    version = 1
)
abstract class DB : RoomDatabase(){

    abstract fun Dao() : DAO

    companion object {

        @Volatile private var instance : DB? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            DB::class.java,
            "menu.db"
        ).build()

    }
}