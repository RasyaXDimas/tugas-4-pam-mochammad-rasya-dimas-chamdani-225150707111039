package com.example.recyclerview.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Menu (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val nama: String,
    val harga: String
)