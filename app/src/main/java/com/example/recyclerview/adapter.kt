package com.example.recyclerview

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.room.DB
import com.example.recyclerview.room.Menu
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class adapter(private val context: Context, private val data:MutableList<Menu>): RecyclerView.Adapter<adapter.MakananViewHolder>() {
    val db by lazy { DB(context) }
    override fun onBindViewHolder(holder: MakananViewHolder, position: Int) {
        val currentItem = data[position]
        holder.tvMakanan.text = currentItem.nama
        holder.tvHarga.text = currentItem.harga
        holder.itemView.setOnClickListener() {
            val intent = Intent(context, detail::class.java)
            intent.putExtra("makanan", currentItem.nama)
            intent.putExtra("harga", currentItem.harga)
            context.startActivity(intent)
        }

        holder.delete.setOnClickListener() {
            val pos = holder.adapterPosition
            if (pos != RecyclerView.NO_POSITION) {
                CoroutineScope(Dispatchers.IO).launch {
                    val deletedItem = data[pos]
                    db.Dao().delMenu(deletedItem)
                    withContext(Dispatchers.Main) {
                        data.removeAt(pos)
                        notifyItemRemoved(pos)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MakananViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item ,parent, false)
        return MakananViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(list: List<Menu>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    class MakananViewHolder(ItemView: View) :
        RecyclerView.ViewHolder(ItemView) {
        var tvMakanan = ItemView.findViewById<TextView>(R.id.tvMakanan)
        var tvHarga = ItemView.findViewById<TextView>(R.id.tvHarga)
        val delete = ItemView.findViewById<ImageButton>(R.id.delete_btn)

    }
}