package com.example.recyclerview

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.room.DB
import com.example.recyclerview.room.Menu
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    private lateinit var adapter: adapter
    private lateinit var black_linear: ImageView
    private lateinit var bg_linear: ImageView
    private lateinit var linear: LinearLayout

    val db by lazy { DB(this) }
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv : RecyclerView = findViewById(R.id.recyclerView)
        val add : Button = findViewById(R.id.add)
        black_linear = findViewById(R.id.black_linear)
        bg_linear = findViewById(R.id.bg_linear)
        linear = findViewById(R.id.linear)
        val nama_add : EditText = findViewById(R.id.nama_add)
        val harga_add : EditText = findViewById(R.id.harga_add)
        val selesai_add : Button = findViewById(R.id.selesai_add)

        val data: MutableList<Menu> = mutableListOf()
        adapter = adapter(this, data)
        rv.adapter = adapter
        rv.layoutManager = LinearLayoutManager(this)

        add.setOnClickListener() {
            showPopUp()
            selesai_add.setOnClickListener() {
                val makanan = nama_add.text.toString()
                val harga = harga_add.text.toString()

                if(makanan.isNotEmpty() && harga.isNotEmpty()) {
                    CoroutineScope(Dispatchers.IO).launch {
                        db.Dao().addMenu(
                            Menu(0, makanan, harga)
                        )
                        val menus = db.Dao().getMenu()
                        withContext(Dispatchers.Main) {
                            adapter.setData(menus)
                        }
                    }
                    hidePopUp()
                    nama_add.setText("")
                    harga_add.setText("")
                } else {
                    Toast.makeText(this@MainActivity, "Makanan dan Harga harus diisi", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        CoroutineScope(Dispatchers.IO).launch {
            val menus = db.Dao().getMenu()
            withContext(Dispatchers.Main) {
                adapter.setData(menus)
            }
        }
    }

    //    ANIMATION
    private fun showPopUp() {
        black_linear.visibility = View.VISIBLE
        bg_linear.visibility = View.VISIBLE
        linear.visibility = View.VISIBLE

        black_linear.transparentIn(1000, 0)
        linear.slideDown(1000, 0)
        bg_linear.slideDown(1000, 0)
    }

    private fun hidePopUp() {
        black_linear.visibility = View.GONE
        bg_linear.visibility = View.GONE
        linear.visibility = View.GONE

        black_linear.transparentOut(1000, 0)
        linear.slideUp(1000, 0)
        bg_linear.slideUp(1000, 0)
    }

    fun View.slideDown(animTime: Long, startOffset: Long) {
        val slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down).apply {
            duration = animTime
            interpolator = FastOutSlowInInterpolator()
            this.startOffset = startOffset
        }
        startAnimation(slideDown)
    }

    fun View.slideUp(animTime: Long, startOffset: Long) {
        val slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up).apply {
            duration = animTime
            interpolator = FastOutSlowInInterpolator()
            this.startOffset = startOffset
        }
        startAnimation(slideUp)
    }

    fun View.transparentIn(animTime: Long, startOffset: Long) {
        val transparentIn = AnimationUtils.loadAnimation(context, R.anim.transparent_in).apply {
            duration = animTime
            interpolator = FastOutSlowInInterpolator()
            this.startOffset = startOffset
        }
        startAnimation(transparentIn)
    }

    fun View.transparentOut(animTime: Long, startOffset: Long) {
        val transparentOut = AnimationUtils.loadAnimation(context, R.anim.transparent_out).apply {
            duration = animTime
            interpolator = FastOutSlowInInterpolator()
            this.startOffset = startOffset
        }
        startAnimation(transparentOut)
    }

}